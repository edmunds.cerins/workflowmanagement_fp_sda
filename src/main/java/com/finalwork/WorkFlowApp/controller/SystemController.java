package com.finalwork.WorkFlowApp.controller;

import com.finalwork.WorkFlowApp.service.DataCollector;
import com.finalwork.WorkFlowApp.service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SystemController {
    @Autowired
    DataService dataService;

    @GetMapping("/data")
    private List<DataCollector> getAllPosition() {
        return dataService.getAllPositions();
    }

    @GetMapping("/data/{id}")
    private DataCollector getPosition(@PathVariable("id") int id) {
        return dataService.getDataById(id);
    }

    @DeleteMapping("/data/{id}")
    private void deletePosition(@PathVariable("id") int id) {
        dataService.delete(id);
    }

    @PostMapping("/data")
    private int savePosition(@RequestBody DataCollector data) {
        dataService.saveOrUpdate(data);
        return data.getPosId();
    }
}
