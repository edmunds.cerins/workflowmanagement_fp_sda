package com.finalwork.WorkFlowApp.repository;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFName;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
public class ExcelReader{

    public static void main(String[] args)
    {
        try
        {
            File file = new File("C:\\Users\\ecerins\\Desktop\\Edmunds\\FINAL_PROJECT\\Data\\PROJECT_1.xlsx");
            FileInputStream fileStream= new FileInputStream(file);
            XSSFWorkbook wb = new XSSFWorkbook(fileStream);
            List<XSSFName> sheetName = wb.getAllNames();
            XSSFSheet sheet = wb.getSheetAt(0);

            System.out.println(sheetName.stream().count());
            Iterator<Row> itr = sheet.iterator();

            while (itr.hasNext())
            {
                Row row = itr.next();
                Iterator<Cell> cellIterator = row.cellIterator();

                while (cellIterator.hasNext())
                {
                    Cell cell = cellIterator.next();

                    switch (cell.getCellType())
                    {
                        case STRING:
                            System.out.print(cell.getStringCellValue() + "\t\t\t");
                            break;
                        case NUMERIC:
                            System.out.print(cell.getNumericCellValue() + "\t\t\t");
                            break;
                        default:
                    }
                }
                System.out.println("");
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}