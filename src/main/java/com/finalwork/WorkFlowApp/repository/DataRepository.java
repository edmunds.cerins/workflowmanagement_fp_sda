package com.finalwork.WorkFlowApp.repository;

import com.finalwork.WorkFlowApp.service.DataCollector;
import org.springframework.data.repository.CrudRepository;

public interface DataRepository extends CrudRepository<DataCollector, Integer> {

}
