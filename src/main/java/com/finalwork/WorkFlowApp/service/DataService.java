package com.finalwork.WorkFlowApp.service;

import com.finalwork.WorkFlowApp.repository.DataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DataService {

        @Autowired
        DataRepository dataRepository;

        public List<DataCollector> getAllPositions() {
            List<DataCollector> dataFields = new ArrayList<DataCollector>();
            dataRepository.findAll().forEach(data -> dataFields.add(data));
            return dataFields;
        }

        public DataCollector getDataById(int id) {
            return dataRepository.findById(id).get();
        }

        public void saveOrUpdate(DataCollector data) {
            dataRepository.save(data);
        }

        public void delete(int id) {
            dataRepository.deleteById(id);
        }
    }
}
