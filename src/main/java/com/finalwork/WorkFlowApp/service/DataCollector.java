package com.finalwork.WorkFlowApp.service;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class DataCollector {

    @Id
    @GeneratedValue
    private int posId;
    private String posDescription;
    private String posSet;
    private Integer posQuantity;
    private Double posPrice;

    public int getPosId() {
        return posId;
    }

    public String getPosDescription() {
        return posDescription;
    }

    public void setPosDescription(String posDescription) {
        this.posDescription = posDescription;
    }

    public String getPosSet() {
        return posSet;
    }

    public void setPosSet(String posSet) {
        this.posSet = posSet;
    }

    public Integer getPosQuantity() {
        return posQuantity;
    }

    public void setPosQuantity(Integer posQuantity) {
        this.posQuantity = posQuantity;
    }

    public Double getPosPrice() {
        return posPrice;
    }

    public void setPosPrice(Double posPrice) {
        this.posPrice = posPrice;
    }

    @Override
    public String toString() {
        return "DataCollector{" +
                "id=" + id +
                ", posDescription='" + posDescription + '\'' +
                ", posSet='" + posSet + '\'' +
                ", posQuantity=" + posQuantity +
                ", posPrice=" + posPrice +
                '}';
    }
}
